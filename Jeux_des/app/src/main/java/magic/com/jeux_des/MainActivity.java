package magic.com.jeux_des;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    // get number of games
    EditText etNumGames;

    TextView number;
    TextView tvRandomDice;
    TextView tvPlayer1Score;
    TextView tvPlayer2Score;
    TextView tvPlayerTurn;
    TextView tvnumber;
    TextView tvPlayerScore1;
    TextView tvPlayerScore2;

    Button btRoll;
    Button btPlay;
    Button btRestart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNumGames = findViewById(R.id.etNum);
        number = findViewById(R.id.textView4);
        tvRandomDice = findViewById(R.id.tvRandomDice);
        tvPlayer1Score = findViewById(R.id.tvPlayer1Score);
        tvPlayer2Score = findViewById(R.id.tvPlayer2Score);
        tvPlayerTurn = findViewById(R.id.tvPlayerTurn);
        tvPlayerScore1 = findViewById(R.id.textView2);
        tvPlayerScore2 = findViewById(R.id.textView3);

        btRoll = findViewById(R.id.button2);
        btPlay = findViewById(R.id.butPlay);
        btRestart = findViewById((R.id.butrestart));
        tvnumber = findViewById(R.id.turns);
        btRoll.setEnabled(false);



    }

    public void butPlay(View view) {
        int numGames = Integer.parseInt(etNumGames.getText().toString());
        if (numGames < 1 || numGames > 10) {
            Toast.makeText(this, "Please select between 1 and 10", Toast.LENGTH_LONG).show();
        } else {
            btRoll.setEnabled(true);
            btPlay.setEnabled(false);

        }
    }


    int player1Total =0;
    int player2Total = 0;
    int playerTurn = 1;
    int numberofturns=0;
    int turnsselected =0;
    public void butRoll(View view) {

        int dice1;
        int dice2;

        int max = 6;
        int min = 1;

        numberofturns += 1;
        turnsselected = Integer.parseInt(etNumGames.getText().toString());
        turnsselected = turnsselected* 2;



        Random r = new Random();
        dice1 = r.nextInt(max - min + 1) + min;
        dice2 = r.nextInt(max - min + 1) + min;
        tvRandomDice.setText("Dice 1: " + dice1 +" " + "Dice 2: " + dice2);

        // making turns between player 1 and 2

        if (playerTurn == 1 )
        {
            player1Total += dice1 + dice2;
            tvPlayerTurn.setText("Player 2 turn to Roll");
            tvPlayerScore2.setTextColor(Color.RED);
            tvPlayerScore1.setTextColor(Color.GRAY);
            playerTurn= 2;
        }
        else if(playerTurn == 2)
        {
            player2Total += dice1 + dice2;
            tvPlayerTurn.setText("Player 1 turn to Roll");
            tvPlayerScore1.setTextColor(Color.RED);
            tvPlayerScore2.setTextColor(Color.GRAY);

            playerTurn = 1;
        }

        // setting player 1 score
        tvPlayer1Score.setText(String.valueOf(player1Total));
        // setting player 2 score
        tvPlayer2Score.setText(String.valueOf(player2Total));

        number.setText(String.valueOf(numberofturns));
        tvnumber.setText(String.valueOf(turnsselected));

        if(numberofturns == turnsselected)
        {

            btRoll.setEnabled(false);

            btRestart.setVisibility(view.VISIBLE);
            if(player1Total > player2Total)
            {
                tvPlayer1Score.setTextColor(Color.RED);
                tvPlayerTurn.setText("Player 1 Wins. Play again?");
                tvPlayerTurn.setTextColor(Color.RED);
                tvPlayerScore1.setTextColor(Color.RED);
                tvPlayerScore2.setTextColor(Color.GRAY);
            }
            else if (player2Total > player1Total )
            {
                tvPlayer2Score.setTextColor(Color.RED);
                tvPlayerTurn.setText("Player 2 Wins. Play Again?");
                tvPlayerTurn.setTextColor(Color.RED);
                tvPlayerScore2.setTextColor(Color.RED);
                tvPlayerScore1.setTextColor(Color.GRAY);
            }
            else if (player2Total == player1Total){
                tvPlayerTurn.setText("TIE. Play Again?");
            }
        }


    }

    public void restart(View view) {
        btPlay.setEnabled(true);
        etNumGames.getText().clear();
        tvRandomDice.setText("");
        tvPlayer2Score.setText("");
        tvPlayer1Score.setText("");
        tvPlayer1Score.setTextColor(Color.GRAY);
        tvPlayer2Score.setTextColor(Color.GRAY);
        tvPlayerTurn.setText("");
        tvnumber.setText("");
         player1Total =0;
        player2Total = 0;
         playerTurn = 1;
         numberofturns=0;
         turnsselected =0;
        number.setText("");
        btRestart.setVisibility(view.INVISIBLE);
    }
}
