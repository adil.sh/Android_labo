package magic.com.loginwithdatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText etUsername;
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsername = findViewById(R.id.etusername);
        etPassword = findViewById(R.id.etpass);
    }

    public void onLogin(View view) {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        String type = "login";
        BackgroundWorker backgroundWorker = new BackgroundWorker(this);


        // execute the background worker class.
        // it takes 3 parameters 1. The type of execution  , 2.Username  and 3. Password
        backgroundWorker.execute(type,username,password);
    }
}
