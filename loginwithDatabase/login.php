<?php
	
	$url = "mysql:host=localhost;dbname=userdb";
	$dbuser = "root";
	$dbpw="";

	$utilUser= $_POST['username'];
	$utilPW = $_POST['password'];
	
try 
{
	$dbcon = new PDO($url,$dbuser,$dbpw);
	$dbcon->setAttribute (PDO::ATTER_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
	$cmd = $dbcon->prepare("select nom,prenom from userdb where username = ? and password = ?;");
	$data = array($utilUser, $utilPW);
	$cmd->execute($data);
	
	$out = "";
	$line;
	while($line = $cmd->fetchObject())
	{
		$out .="$line->nom"." "."$line->prenom"; 
	}
	
}
catch (Exception $ex)
{
	$out = $ex->getMessage();
}

echo $out;
?>