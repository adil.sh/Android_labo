package magic.com.lab4_sqlite;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ajouterRDV extends AppCompatActivity {

    EditText et_idRDV;
    Button bt_ajouterRDV;
    TextView tv_date;
    Calendar mCurrentDate;
    int day,month,year;

    MyDBAdapter mydb;
    String getdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_rdv);

        et_idRDV = findViewById(R.id.et_idRDV);
        bt_ajouterRDV = findViewById(R.id.bt_ajouterRDV);
        tv_date = findViewById(R.id.tv_date);
        mCurrentDate = Calendar.getInstance();

        day = mCurrentDate.get(Calendar.DAY_OF_MONTH);
        month = mCurrentDate.get(Calendar.MONTH);
        year = mCurrentDate.get(Calendar.YEAR);


        getdate = day+"/"+month+"/"+year;
        tv_date.setText(getdate);

        tv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(ajouterRDV.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        tv_date.setText(i + "/" + i1 + "/" + i2 );
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        mydb = new MyDBAdapter(this);

        ajouterRDV();
    }

    public void ajouterRDV()
    {
        bt_ajouterRDV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Boolean isRDVtaken = mydb.insertRDV(et_idRDV.getText().toString(),tv_date.getText().toString());

                if(isRDVtaken== true)
                {
                    Toast.makeText(ajouterRDV.this, "Data inserted", Toast.LENGTH_LONG).show();
                }
                else if (isRDVtaken == false)
                {
                    Toast.makeText(ajouterRDV.this, "Error", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
