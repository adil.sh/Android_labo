package magic.com.lab4_sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

       @Override
    public boolean  onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id==R.id.id_afficher)
        {
            // write logic here
            Intent i = new Intent(MainActivity.this, AfficherEtudiants.class);
            startActivity(i);
            return true;
        }
        else if (id==R.id.id_ajouter)
        {
            Intent i = new Intent(MainActivity.this, ajouterEtudiant.class);
            startActivity(i);
            return true;
        }
        else if (id==R.id.id_modifier)
        {
            Intent i = new Intent(MainActivity.this, modifier_etudiant.class);
            startActivity(i);
            return true;
        }
        else if (id==R.id.id_supprimer)
        {
            Intent i = new Intent(MainActivity.this, supprimerEtudiant.class);
            startActivity(i);
            return true;
        }
        else if (id==R.id.id_afficherRDV)
        {
            return true;
        }
        else if (id==R.id.id_ajouterRDV)
        {
            return true;
        }

        return true;
    }


    public void bt_ajouter(View view) {
        Intent i = new Intent(MainActivity.this, ajouterEtudiant.class);
        startActivity(i);
    }

    public void bt_afficher(View view) {
        Intent i = new Intent(MainActivity.this, AfficherEtudiants.class);
        startActivity(i);
    }

    public void bt_modifier(View view) {
        Intent i = new Intent(MainActivity.this, modifier_etudiant.class);
        startActivity(i);
    }

    public void bt_supprimer(View view) {
        Intent i = new Intent(MainActivity.this, supprimerEtudiant.class);
        startActivity(i);
    }

    public void bt_ajouterrdv(View view) {
        Intent i = new Intent(MainActivity.this, ajouterRDV.class);
        startActivity(i);
    }

    public void bt_afficherRDV(View view) {
        Intent i = new Intent(MainActivity.this, afficher_rdv.class);
        startActivity(i);
    }
}
