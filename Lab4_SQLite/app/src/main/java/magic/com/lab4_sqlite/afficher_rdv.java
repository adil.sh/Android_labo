package magic.com.lab4_sqlite;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class afficher_rdv extends AppCompatActivity {

        EditText et_afficherRDV;
        Button bt_afficherRDV;
        MyDBAdapter mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afficher_rdv);

        et_afficherRDV = findViewById(R.id.et_idRDV);
        bt_afficherRDV = findViewById(R.id.bt_afficherRDV);
        mydb = new MyDBAdapter(this);

        afficherRDVData();

    }

    public void afficherRDVData()
    {
        bt_afficherRDV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor res = mydb.getRDVdata(et_afficherRDV.getText().toString());
                if(res.getCount() == 0)
                {
                    // if there is nothing in the data base
                    showMessage("Error", "No data Found");
                    return;
                }
                StringBuffer buffer = new StringBuffer();

                // create a while loop to put all the data in the stringbuffer
                while(res.moveToNext())
                {
                    buffer.append("ID RDV : " +  res.getString(0) + "\n");
                    buffer.append("Date RDV  : " + res.getString(1)+ "\n");
                    buffer.append("ID ETUDIANT: " + res.getString(2)+ "\n");
                    buffer.append("\t \t \t ------------ \t \n \n");


                }

                showMessage("Data", buffer.toString());
                // show all data
            }
        });
    }

    public void showMessage (String title, String Message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();

    }

}
