package magic.com.lab4_sqlite;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class AfficherEtudiants extends AppCompatActivity {

    Button bt_Afficher;
    MyDBAdapter mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afficher_etudiants);

        bt_Afficher = findViewById(R.id.bt_Afficher);

        mydb = new MyDBAdapter(this);

        afficherData();
    }

    public void afficherData()
    {
        bt_Afficher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor res = mydb.getAllData();
                if(res.getCount() == 0)
                {
                    // if there is nothing in the data base
                    showMessage("Error", "No data Found");
                    return;
                }
                StringBuffer buffer = new StringBuffer();

                // create a while loop to put all the data in the stringbuffer
                while(res.moveToNext())
                {
                    buffer.append("ID : " +  res.getString(0) + "\n");
                    buffer.append("Nom : " + res.getString(1)+ "\n");
                    buffer.append("Prenom : " + res.getString(2)+ "\n");
                    buffer.append("Programme : " + res.getString(3)+ "\n");
                    buffer.append("Cours : " + res.getString(4)+ "\n");
                    buffer.append("Nombre d'heure : " + res.getString(5)+ "\n ");
                    buffer.append("\t \t \t ------------ \t \n \n");


                }

                showMessage("Data", buffer.toString());
                // show all data

            }

        });
    }

    //Dialog box to show all students
    public void showMessage (String title, String Message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.id_afficher) {
            // write logic here
            Intent i = new Intent(AfficherEtudiants.this, AfficherEtudiants.class);
            startActivity(i);
            return true;
        } else if (id == R.id.id_ajouter) {
            Intent i = new Intent(AfficherEtudiants.this, ajouterEtudiant.class);
            startActivity(i);
            return true;
        } else if (id == R.id.id_modifier) {
            Intent i = new Intent(AfficherEtudiants.this, modifier_etudiant.class);
            startActivity(i);
            return true;

        } else if (id == R.id.id_afficherRDV) {
            return true;
        } else if (id == R.id.id_ajouterRDV) {
            return true;
        }

        return true;
    }

}
