package magic.com.lab4_sqlite;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBAdapter extends SQLiteOpenHelper {

    //1. Need to create a database name
    public static final String database_name = "etudiantsDatardv.db";
    //2. Create a table name
    public static final String table_name = "Table_Etudiant";
    public static final String table_rdv = "Table_rdv_etudiant";
    //3. Create the column names
    public static final String col_1 = "ID";
    public static final String col_2 = "Nom";
    public static final String col_3 = "Prenom";
    public static final String col_4 = "Programme";
    public static final String col_5 = "Cours";
    public static final String col_6 = "NbHeures";

    public static final String rdvCol_1 = "IDRDV";
    public static final String rdvCol_2 = "id_etudiant";
    public static final String rdvCol_3 = "Date_RDV";

    public static final int db_version = 1;

    public MyDBAdapter(Context context) {
        super(context, database_name, null, db_version);

    }

    // create table method
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // on create is used to create a query when the method is called.
        // we will create our table in this
        sqLiteDatabase.execSQL("create table " + table_name + "(ID integer primary key autoincrement, Nom text, Prenom text, Programme text, Cours text, NbHeures text )");

        sqLiteDatabase.execSQL("create table " + table_rdv + "(IDRDV integer primary key autoincrement, Date_RDV text, id_etudiant integer ,FOREIGN KEY (id_etudiant) REFERENCES Table_Etudiant(id_etudiant))");
    }



    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + table_name);

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + table_rdv);
        onCreate(sqLiteDatabase);
    }

     // ADD A NEW ROW TO THE DATABASE

     public boolean insertEtudiant(String nom, String prenom, String programme, String cours, String NbHeures) {
         ContentValues contentValues = new ContentValues();

         SQLiteDatabase db = getWritableDatabase();

         contentValues.put(col_2, nom);
         contentValues.put(col_3, prenom);
         contentValues.put(col_4, programme);
         contentValues.put(col_5, cours);
         contentValues.put(col_6, NbHeures);



         Long result = db.insert(table_name, null, contentValues);
         if(result == -1)
         {
             return false;
         }
         else
         {
             return true;
         }
     }

    //USED TO RETRIEVE ALL THE DATA FROM THE DATABASE
    public Cursor getAllData() {
        // create instance of database class
        SQLiteDatabase db = this.getWritableDatabase();
        // create an instance of cursor class
        Cursor res = db.rawQuery("select * from " + table_name, null);
        return res;

    }

    //USED TO UPDATE THE DATA

    public boolean updateData(String id,String nom, String prenom, String programme, String cours, String NbHeures)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(col_1, id);
        contentValues.put(col_2, nom);
        contentValues.put(col_3, prenom);
        contentValues.put(col_4, programme);
        contentValues.put(col_5, cours);
        contentValues.put(col_6, NbHeures);

        int result = db.update(table_name, contentValues, " ID = ?", new String[] {id} );
        if(result == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    public boolean deleteData(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(col_1, id);

        int result =  db.delete(table_name, " ID = ?", new String[] {id});
        if(result == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // insert rdv

    public boolean insertRDV(String id_etudiant, String Date_RDV) {
        ContentValues contentValues = new ContentValues();

        SQLiteDatabase db = getWritableDatabase();

        contentValues.put(rdvCol_2, id_etudiant);
        contentValues.put(rdvCol_3, Date_RDV);




        Long result = db.insert(table_rdv, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // afficher les rdv
    public Cursor getRDVdata(String id) {
        // create instance of database class
        SQLiteDatabase db = this.getWritableDatabase();
        // create an instance of cursor class
        Cursor res = db.rawQuery("select * from " + table_rdv + " where id_etudiant =" + id, null);
        return res;

    }
 }
