package adil.com.canvas_accelerometer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private canvasView canvas;
    private int circleRadius = 30;
    private float circleX;
    private float circleY;
    private Timer timer;
    private Handler handler;

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private float sensorX;
    private float sensorY;
    private float sensorZ;

    private long lastSensorUpdateTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        sensorManager.registerListener(this,accelerometer,SensorManager.SENSOR_DELAY_NORMAL);

        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        int screenWidth = size.x;
        int screenHeight = size.y;

        circleX = screenWidth / 2 - circleRadius;
        circleY = screenHeight / 2 - circleRadius;

        canvas = new canvasView(MainActivity.this);
        setContentView(canvas);

        handler = new Handler(){
            @Override
            public void handleMessage(Message message){
                canvas.invalidate();
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                if(sensorX<0)
                {
                    circleX += 10;
                }else {
                    circleX -= 10;
                }

                if(sensorY > 0 )
                {
                    circleY +=10;
                } else {
                    circleY -=10;
                }

                handler.sendEmptyMessage(0);
            }
        }, 0, 100);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if(mySensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {
            long currentTime = System.currentTimeMillis();

            if((currentTime - lastSensorUpdateTime) > 100) {
                sensorX = sensorEvent.values[0];
                sensorY = sensorEvent.values[1];
                sensorZ = sensorEvent.values[2];
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // will not be used
    }

    //
    private class canvasView extends View {
        private Paint pen;
        public canvasView(Context context) {
            super(context);

            setFocusable(true);

            pen = new Paint();

        }

        public void onDraw(Canvas screen)
        {
            pen.setStyle(Paint.Style.FILL);
            pen.setAntiAlias(true);
            pen.setTextSize(30f);
            pen.setColor(Color.WHITE);
            Drawable d = getResources().getDrawable(R.drawable.bk);
            d.setBounds(0, 1900, 1500, 0);
            d.draw(screen);

            screen.drawCircle(circleX,circleY,circleRadius,pen);
        }

    }
}
