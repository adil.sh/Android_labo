package magic.com.tictactoe;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buclick(View view) {
        Button bt = (Button) view;


        int butID = 0;

        switch (bt.getId())
        {
            case R.id.bu1:
                butID = 1;
                break;
            case R.id.bu2:
                butID = 2;
                break;
            case R.id.bu3:
                butID = 3;
                break;
            case R.id.bu4:
                butID = 4;
                break;
            case R.id.bu5:
                butID = 5;
                break;
            case R.id.bu6:
                butID = 6;
                break;
            case R.id.bu7:
                butID = 7;
                break;
            case R.id.bu8:
                butID = 8;
                break;
            case R.id.bu9:
                butID = 9;
                break;

        }

        playGame(butID, bt);

    }
    int playerturn = 1; // 1 for player 1 and 2 for player 2
    ArrayList<Integer> player1  = new ArrayList<Integer>(); // Hold player 1 data
    ArrayList<Integer> player2 = new ArrayList<Integer>(); // Hold player 2 data


    void playGame(int cellId, Button bt)
    {
        Log.d("Player: ", String.valueOf(cellId));

        if(playerturn == 1)
        {
            bt.setText("O");
            bt.setBackgroundColor(Color.RED);
            player1.add(cellId);
            playerturn =2 ;

        }
        else if(playerturn == 2)
        {
            bt.setText("X");
            bt.setBackgroundColor(Color.GREEN);
            player2.add(cellId);
            playerturn =1 ;
        }
        bt.setEnabled(false);
        checkWinner();
    }

    void checkWinner() {
        int winner = -1;

        // set Row 1
        if (player1.contains(1) && player1.contains(2) && player1.contains(3)) {
            winner = 1;
        }
        if (player2.contains(1) && player2.contains(2) && player2.contains(3)) {
            winner = 2;
        }
        //set Row 2
        if (player1.contains(4) && player1.contains(5) && player1.contains(6)) {
            winner = 1;
        }
        if (player2.contains(4) && player2.contains(5) && player2.contains(6)) {
            winner = 2;
        }
        // set row 3

        if (player1.contains(7) && player1.contains(8) && player1.contains(9)) {
            winner = 1;
        }
        if (player2.contains(7) && player2.contains(8) && player2.contains(9)) {
            winner = 2;
        }
        // Column 1
        if (player1.contains(1) && player1.contains(4) && player1.contains(9)) {
            winner = 1;
        }
        if (player2.contains(1) && player2.contains(4) && player2.contains(9)) {
            winner = 2;
        }
        // col 2
        if (player1.contains(2) && player1.contains(5) && player1.contains(8)) {
            winner = 1;
        }
        if (player2.contains(2) && player2.contains(5) && player2.contains(8)) {
            winner = 2;
        }
        // col 3
        if (player1.contains(3) && player1.contains(6) && player1.contains(7)) {
            winner = 1;
        }
        if (player2.contains(3) && player2.contains(6) && player2.contains(7)) {
            winner = 2;
        }
            // col + row 1
            if (player1.contains(3) && player1.contains(5) && player1.contains(9)) {
                winner = 1;
            }
            if (player2.contains(3) && player2.contains(5) && player2.contains(9)) {
                winner = 2;
            }
            // col and row 2
            if (player1.contains(1) && player1.contains(5) && player1.contains(7)) {
                winner = 1;
            }
            if (player2.contains(1) && player2.contains(5) && player2.contains(7)) {
                winner = 2;
            }

            if (winner != 0) {
                if (winner == 1) {
                    Toast.makeText(this, "Player 1 won", Toast.LENGTH_LONG).show();
                }
                if (winner == 2) {
                    Toast.makeText(this, "Player 1 won", Toast.LENGTH_LONG).show();
                }
            }


        }
    }
