package magic.com.appfindmyage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText et1;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et1 = findViewById(R.id.edob);
        tvResult = findViewById(R.id.result);
    }

    public void butFind(View view) {
        final int year = 2018;
        int dob = Integer.parseInt(et1.getText().toString());


        // get device current time
        Calendar calendar = Calendar.getInstance();

        int year1 = calendar.get(calendar.YEAR);
        int age = year1 - dob;
        tvResult.setText("You are " + age + " years old");
    }
}
